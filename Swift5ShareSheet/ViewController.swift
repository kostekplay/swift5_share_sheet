////  ViewController.swift
//  Swift5ShareSheet
//
//  Created on 18/10/2020.
//  
//

// zapamiętywanie w share sheet fotek wymaga ustaenia e info.plist zgody na zpamietywanie fotek.
// Privacy - Photo Library Additions Usage Description

import UIKit

class ViewController: UIViewController {
    
    private let button: UIButton = {
        let b = UIButton()
        b.backgroundColor = .systemGreen
        b.setTitle("Tap Me", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.layer.cornerRadius = 9
        b.layer.masksToBounds = true
        b.frame = CGRect(x: 0, y: 0, width: 220, height: 55)
        b.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.center = view.center
        view.addSubview(button)
    }
    
    @objc private func didTapButton() {
        
        guard let image = UIImage(systemName: "bell"), let url = URL(string: "https://www.google.com") else { return }
        
        let shareSheetVC = UIActivityViewController(activityItems: [
            image,
            url
        ],
        applicationActivities: nil)
        
        present(shareSheetVC, animated: true)
    }
}

